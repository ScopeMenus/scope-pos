<?php

namespace Modules\Order\DataTables;

use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('type', function ($data) {
                return __($data->type);
            })
            ->addColumn('action', function ($data) {
                return view('order::partials.actions', compact('data'));
            });
    }

    public function query(Order $model)
    {
        return $model->newQuery()->with('cashier');
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('order-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom("<'row'<'col-md-3'l><'col-md-5 mb-2'B><'col-md-4'f>> .
                                'tr' .
                                <'row'<'col-md-5'i><'col-md-7 mt-2'p>>")
                    ->orderBy(7)
                    ->buttons(
                        Button::make('excel')
                            ->text('<i class="bi bi-file-earmark-excel-fill"></i> Excel'),
                        Button::make('print')
                            ->text('<i class="bi bi-printer-fill"></i> Print'),
                        Button::make('reset')
                            ->text('<i class="bi bi-x-circle"></i> Reset'),
                        Button::make('reload')
                            ->text('<i class="bi bi-arrow-repeat"></i> Reload')
                    );
    }

    protected function getColumns()
    {
        return [
            Column::computed('type')
                ->title(__('Type'))
                ->className('text-center align-middle'),

            Column::computed('cashier.name')
                ->title(__('Cashier'))
                ->className('text-center align-middle'),

            Column::make('tax_amount')
                ->title(__('Tax'))
                ->className('text-center align-middle'),

            Column::make('discount_amount')
                ->title(__('Discount'))
                ->className('text-center align-middle'),

            Column::computed('shipping_amount')
                ->title(__('Shipping'))
                ->className('text-center align-middle'),

            Column::computed('sub_total_amount')
                ->title(__('Subtotal'))
                ->className('text-center align-middle'),

            Column::make('total_amount')
                ->title(__('Total'))
                ->className('text-center align-middle'),

            Column::computed('action')
                ->title(__('Actions'))
                ->exportable(false)
                ->printable(false)
                ->className('text-center align-middle'),

            Column::make('created_at')
                ->visible(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
