<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cashier_id')->nullable()->references('id')->on('users')->nullOnDelete();
            $table->foreignId('customer_id')->nullable()->constrained();
            $table->foreignId('address_id')->nullable()->constrained();
            $table->unsignedBigInteger('table_id')->nullable();
            $table->foreignId('delivery_id')->nullable()->references('id')->on('users')->nullOnDelete();
            $table->string('phone')->nullable();
            $table->integer('tax_amount')->default(0);
            $table->float('discount_amount', 10, 2)->default(0);
            $table->float('shipping_amount', 10, 2)->default(0);
            $table->float('sub_total_amount', 10, 2);
            $table->float('total_amount', 10, 2);
            $table->string('type')->nullable();
            $table->string('payment_method')->nullable();
            $table->timestamp('collected_at')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
