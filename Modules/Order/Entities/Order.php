<?php

namespace Modules\Order\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\People\Entities\Address;
use Modules\People\Entities\Customer;
use Modules\Product\Entities\Product;

class Order extends Model
{
    use HasFactory;

    const ORDER_TYPES = [
        'TIK_AWAY'  => 'tik_away',
        'DELIVERY'  => 'delivery',

    ];

    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_details')
            ->withPivot('product_name', 'quantity', 'price', 'sub_total')
            ->withTimestamps();
    }

    public function cashier()
    {
        return $this->belongsTo(User::class, 'cashier_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
