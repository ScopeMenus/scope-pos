<?php

namespace Modules\Order\Http\Controllers\Api;

use App\Models\User;
use App\Services\RespondActive;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\Entities\Order;
use Modules\Order\Http\Requests\Api\AssignOrderToDeliveryRequest;
use Modules\Order\Http\Requests\Api\SetOrdersRequest;
use Modules\Order\Http\Resources\OrderResource;
use Modules\People\Entities\Address;
use Modules\Product\Entities\Product;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders =
        Order::whereTypeAndBranchId('delivery', auth()->user()->branch_id)
        ->whereDate('created_at', today())
        ->whereNull(['canceled_at', 'collected_at'])
        ->with('customer', 'products', 'address')
        ->when($request->is_assigned == 1, function ($query) use ($request) {
            $query->whereNotNull('delivery_id');
        })
        ->when($request->is_assigned == 0, function ($query) use ($request) {
            $query->whereNull('delivery_id');
        })
        ->latest('id')
        ->get();

        $data['orders'] = OrderResource::collection($orders);

        $data['deliveries'] = User::whereTypeAndIsBusy('delivery', 0)->get();

        return RespondActive::success('Order done successfully.', $data);
    }

    public function store(SetOrdersRequest $request)
    {
        $address = Address::find($request->address_id);

        $order = Order::create([
            'cashier_id'        => auth()->id(),
            'customer_id'       => $request->customer_id,
            'address_id'        => $request->address_id,
            'phone'             => $request->phone,
            'tax_amount'        => $request->tax,
            'discount_amount'   => $request->discount,
            'shipping_amount'   => optional($address)->shipping ?: 0,
            'sub_total_amount'  => $request->sub_total,
            'total_amount'      => $request->total,
            'type'              => $request->type,
            'payment_method'    => 'cash',
            'notes'             => $request->notes,
            'collected_at'      => $request->type == 'take-away' ? now() : null
        ]);

        $products = [];
        foreach ($request['products'] as $product) {
            $products[ $product['id'] ] =
                [
                    'product_name'              => $product['name'],
                    'quantity'                  => $product['qty'],
                    'price'                     => $product['price'],
                    'sub_total'                 => $product['price'] * $product['qty'],
                ];

//            $product = Product::find($product['id']);
//
//            $product->decrement($product['qty']);
//
//            $product->increament($product['qty']);
        }

        $order->products()->attach($products);

        $orderHtml = '<!DOCTYPE html><html lang="en"><head> <style> body { margin: 0; padding: 0; } @page { size: 2.8in 11in; margin-top: 0cm; margin-left: 0cm; margin-right: 0cm; } table { width: 100%; } tr { width: 100%; } h1 { text-align: center; vertical-align: middle; } #logo { width: 60%; text-align: center; -webkit-align-content: center; align-content: center; padding: 5px; margin: 2px; display: block; margin: 0 auto; } header { width: 100%; text-align: center; -webkit-align-content: center; align-content: center; vertical-align: middle; } .items thead { text-align: center; } .center-align { text-align: center; } .bill-details td { font-size: 12px; } .receipt { font-size: medium; } .items .heading { font-size: 12.5px; text-transform: uppercase; border-top:1px solid black; margin-bottom: 4px; border-bottom: 1px solid black; vertical-align: middle; } .items thead tr th:first-child, .items tbody tr td:first-child { width: 47%; min-width: 47%; max-width: 47%; word-break: break-all; text-align: left; } .items td { font-size: 12px; text-align: right; vertical-align: bottom; } .price::before { content: "\20B9"; font-family: Arial; text-align: right; } .sum-up { text-align: right !important; } .total { font-size: 13px; border-top:1px dashed black !important; border-bottom:1px dashed black !important; } .total.text, .total.price { text-align: right; } .total.price::before { content: "\20B9"; } .line { border-top:1px solid black !important; } .heading.rate { width: 20%; } .heading.amount { width: 25%; } .heading.qty { width: 5% } p { padding: 1px; margin: 0; } section, footer { font-size: 12px; } </style></head><body><header> <div id="logo" class="media" data-src="logo.png" src="./logo.png"></div></header><p>GST Number : 4910487129047124</p><table class="bill-details"> <tbody> <tr> <td>Date : <span>1</span></td> <td>Time : <span>2</span></td> </tr> <tr> <td>Table #: <span>3</span></td> <td>Bill # : <span>4</span></td> </tr> <tr> <th class="center-align" colspan="2"><span class="receipt">Original Receipt</span></th> </tr> </tbody></table><table class="items"> <thead>'; foreach ($request['products'] as $product) { $orderHtml.= '<tr> <th class="heading name">'.$product['name'].'</th> <th class="heading qty">'.$product['qty'].'</th> <th class="heading rate">'.$product['price'].'</th> <th class="heading amount">'.$product['price'] * $product['qty'].'</th> </tr>'; } $orderHtml .= ' </thead> <tbody> <tr> <td>Chocolate milkshake frappe</td> <td>1</td> <td class="price">200.00</td> <td class="price">200.00</td> </tr> <tr> <td colspan="3" class="sum-up line">Subtotal</td> <td class="line price">12112.00</td> </tr> <tr> <td colspan="3" class="sum-up">CGST</td> <td class="price">10.00</td> </tr> <tr> <td colspan="3" class="sum-up">SGST</td> <td class="price">10.00</td> </tr> <tr> <th colspan="3" class="total text">Total</th> <th class="total price">12132.00</th> </tr> </tbody></table><section> <p> Paid by : <span>CASH</span> </p> <p style="text-align:center"> Thank you for your visit! </p></section><footer style="text-align:center"> <p>Technology Partner Dotworld Technologies</p> <p>www.dotworld.in</p></footer></body></html>';

        return RespondActive::success('Order done successfully.', $orderHtml);
    }

    public function update(Request $request, Order $order)
    {
        $order->update([
            'customer_id'       => $request->customer_id,
            'address_id'        => $request->address_id,
            'phone'             => $request->phone,
            'tax_amount'        => $request->tax,
            'discount_amount'   => $request->discount,
            'sub_total_amount'  => $request->sub_total,
            'total_amount'      => $request->total,
            'type'              => $request->type,
            'payment_method'    => 'cash',
            'notes'             => $request->notes,
        ]);

        $order->products()->detach();

        $products = [];
        foreach ($request['products'] as $product) {
            $products[ $product['id'] ] =
                [
                    'product_name'              => $product['name'],
                    'quantity'                  => $product['qty'],
                    'price'                     => $product['price'],
                    'sub_total'                 => $product['price'] * $product['qty'],
                ];
        }

        $order->products()->attach($products);

        return RespondActive::success('Order updated successfully.');
    }

    public function assignOrderToDelivery(AssignOrderToDeliveryRequest $request)
    {
        $delivery = User::whereTypeAndId('delivery', $request->delivery_id)->first();

        $order = Order::find($request->order_id);

        if ($request->is_collected == 1) {
            $order->update([ 'collected_at'  => now() ]);

            $delivery->update([ 'is_busy' => 0 ]);
        }else {

            $order->update([ 'delivery_id' => $request->delivery_id ]);

            $delivery->update([ 'is_busy' => 1 ]);
        }

        return RespondActive::success('This action ran successfully.');
    }

    public function cancelOrder(Request $request)
    {
        $order = Order::find($request->order_id);

        $order->update([ 'canceled_at' => now() ]);

        return RespondActive::success('This action ran successfully.');
    }
}
