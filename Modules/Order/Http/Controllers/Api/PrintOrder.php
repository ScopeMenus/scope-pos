<?php

namespace Modules\Order\Http\Controllers\Api;

use App\Services\RespondActive;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Modules\Order\Entities\Order;

class PrintOrder extends Controller
{
//    public function __invoke(Order $order)
//    {
//        $pdf = 'pdff.pdf';
//        $connector = new FilePrintConnector("php://stdout");
//        $printer = new Printer($connector);
//        $pages = ImagickEscposImage::loadPdf($pdf);
//        foreach ($pages as $page) {
//            $printer -> graphics($page);
//        }
//        $printer -> cut();
//        $printer -> close();
//    }
    public function __invoke(Order $order)
    {
        $order->load('products');

        $connector = new WindowsPrintConnector("80mm Series Printer");

        $printer = new Printer($connector);

        /* Name of shop */
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->text("ExampleMart Ltd.\n");
        $printer->selectPrintMode();
        $printer->text("Shop No. 42.\n");

        $printer->feed();
        $printer->setPrintLeftMargin(0);
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->setEmphasis(true);
        $printer->text(
            $this->addSpaces('Item', 20) .
            $this->addSpaces('Qty', 8) .
            $this->addSpaces('Price', 12) .
            $this->addSpaces('Total', 8) . "\n"
        );
        $printer->text("----------------------------------------------\n");
        $printer->setEmphasis(false);

        foreach ($order->products as $item) {

            //Current item ROW 1
            $name_lines = str_split($item->pivot->product_name, 15);
            foreach ($name_lines as $k => $l) {
                $l = trim($l);
                $name_lines[$k] = $this->addSpaces($l, 20);
            }

            $qty = str_split($item->pivot->quantity, 10);
            foreach ($qty as $k => $l) {
                $l = trim($l);
                $qty[$k] = $this->addSpaces($l, 8);
            }

            $price = str_split($item->pivot->price, 10);
            foreach ($price as $k => $l) {
                $l = trim($l);
                $price[$k] = $this->addSpaces($l, 12);
            }

            $total_price = str_split($item->pivot->sub_total, 8);
            foreach ($total_price as $k => $l) {
                $l = trim($l);
                $total_price[$k] = $this->addSpaces($l, 8);
            }

            $temp = [];
            $temp[] = count($name_lines);
            $temp[] = count($qty);
            $temp[] = count($price);
            $temp[] = count($total_price);
            $counter = max($temp);

            for ($i = 0; $i < $counter; $i++) {
                $line = '';
                if (isset($name_lines[$i])) {
                    $line .= ($name_lines[$i]);
                }
                if (isset($qty[$i])) {
                    $line .= ($qty[$i]);
                }
                if (isset($price[$i])) {
                    $line .= ($price[$i]);
                }
                if (isset($total_price[$i])) {
                    $line .= ($total_price[$i]);
                }
                $printer->text($line . "\n");
            }

            $printer->feed();
        }

        /* Footer */
        $date = "Monday 6th of April 2015 02:56:25 PM";
        $printer->feed(2);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("Thank you for shopping at ExampleMart\n");
        $printer->text("For trading hours, please visit example.com\n");
        $printer->feed(2);
        $printer->text($date . "\n");

        $printer->cut();
        $printer->pulse();
        $printer->close();

        return RespondActive::success('This action ran successfully!');
    }

    public function addSpaces($string = '', $valid_string_length = 0) {
        if (strlen($string) < $valid_string_length) {
            $spaces = $valid_string_length - strlen($string);
            for ($index1 = 1; $index1 <= $spaces; $index1++) {
                $string = $string . ' ';
            }
        }

        return $string;
    }

}
