<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\DataTables\OrderDataTable;
use Modules\Order\Entities\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(OrderDataTable $dataTable)
    {
        return $dataTable->render('order::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('order::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
//        Order::create([
//            'reference' => "asd",
//            'customer_id' => $request->user_id,
//            'table_id' => $request->table_id,
//            'total_amount' => $request->total,
//            'tax_amount' => $request->tax,
//            'shipping_amount' => $request->sub_total,
//            'service_amount' => $request->service,
//            'discount_amount' => $request->discount,
//            'payment_status' => $request->status,
//            'payment_method' => $request->type,
//            'reference' => $request->address_id,
//        ]);
//
//        return 'success';
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Order $order)
    {
        $order->load('cashier', 'address');

        return view('order::show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('order::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
