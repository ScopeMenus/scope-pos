<?php

namespace Modules\Order\Http\Requests\Api;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SetOrdersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'                       => ['nullable', 'exists:customers,id'],
            'address_id'                        => ['nullable', 'exists:addresses,id'],
            'phone'                             => ['required_if:type,delivery'],
            'tax'                               => ['required', 'numeric', 'min:1', 'max:99'],
            'discount'                          => ['nullable', 'numeric', 'min:0', 'max:9999'],
            'shipping'                          => ['nullable', 'numeric', 'min:0', 'max:999999'],
            'sub_total'                         => ['required', 'numeric', 'min:1', 'max:99999999'],
            'total'                             => ['required', 'numeric', 'min:1', 'max:99999999'],
            'type'                              => ['nullable', 'in:take-away,delivery'],
            'payment_method'                    => ['nullable', 'string', 'max:200'],
            'notes'                             => ['nullable', 'string', 'max:1500'],
            'products'                          => ['required', 'array', 'distinct', 'min:1'],
            'products.*.id'                     => ['required', 'exists:products,id'],
            'products.*.name'                   => ['required', 'string', 'max:250'],
            'products.*.qty'                    => ['required', 'min:1', 'max:999999'],
            'products.*.price'                  => ['required', 'min:1', 'max:999999'],
            'products.*.attribute_parent_id'    => ['nullable', 'exists:attribute_products,id,translation_id,NULL,parent_id,NULL'],
            'products.*.attribute_child_id'     => ['nullable', 'exists:attribute_products,id,translation_id,NULL,parent_id,!NULL'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
