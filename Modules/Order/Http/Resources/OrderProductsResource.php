<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->pivot->product_name,
            'quantity'      => $this->pivot->quantity,
            'price'         => $this->pivot->price,
            'subtotal'      => $this->pivot->subtotal,
            'additions'     => []
        ];
    }
}
