<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\People\Http\Resources\CustomerResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'address_id'    => $this->address_id,
            'delivery_id'   => $this->delivery_id,
            'type'          => $this->type,
            'phone'         => $this->phone,
            'discount'      => $this->discount_amount,
            'tax'           => $this->tax_amount,
            'shipping'      => $this->shipping_amount,
            'subtotal'      => $this->sub_total_amount,
            'total'         => $this->total_amount,
            'is_collected'  => $this->is_collected,
            'notes'         => $this->notes,
            'created_at'    => $this->created_at->format('Y-m-d H:i'),
            'customer'      => new CustomerResource(optional($this->customer)->load('addresses')),
            'products'      => OrderProductsResource::collection($this->products),
        ];
    }
}
