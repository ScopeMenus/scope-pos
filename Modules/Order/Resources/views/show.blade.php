@extends('layouts.app')

@section('title', __('Order Details'))

@section('breadcrumb')
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{__('Home')}}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('orders.index') }}">{{__('Orders')}}</a></li>
        <li class="breadcrumb-item active">{{__('Details')}}</li>
    </ol>
@endsection

@section('content')
    <div class="container-fluid mb-4">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mb-0">
                                <tr>
                                    <th>{{ __('Order number') }}</th>
                                    <td>{{ $order->id }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Cashier') }}</th>
                                    <td>{{ $order->cashier->name  }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Type') }}</th>
                                    <td>{{ __($order->type) }}</td>
                                </tr>
                                @if($order->type == 'delivery')
                                <tr>
                                    <th>{{ __('Address') }}</th>
                                    <td>{{ optional($order->address)->full_address }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Phone') }}</th>
                                    <td>{{ $order->phone }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>{{ __('Tax') }} (%)</th>
                                    <td>{{ $order->tax_amount }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Discount') }}</th>
                                    <td>{{ $order->discount_amount }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Shipping') }}</th>
                                    <td>{{ $order->shipping_amount }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Subtotal') }}</th>
                                    <td>{{ $order->sub_total_amount }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Total') }}</th>
                                    <td>{{ $order->total_amount }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Is collected') }}</th>
                                    <td>{{ $order->is_collected == 1 ? __('True') : __('False') }}</td>
                                </tr>
                                <tr>
                                    <th>{{ __('Notes') }}</th>
                                    <td>{{ $order->notes }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered table-striped text-center mb-0">
                            <thead>
                            <tr>
                                <th>{{__('Product name')}}</th>
                                <th>{{__('Quantity')}}</th>
                                <th>{{__('Price')}}</th>
                                <th>{{__('Subtotal')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($order->products as $product)
                                <tr>
                                    <td>{{ $product->product_name }}</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->sub_total }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8">
                                        <span class="text-danger">{{__('No Purchases Data Available!')}}</span>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



