<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/order', function (Request $request) {
    return $request->user();
});

Route::apiResource('orders', 'Api\OrderController');
Route::middleware('auth:sanctum')->group(function() {
    Route::post('orders/cancel', 'Api\OrderController@cancelOrder');
    Route::post('assign_order_delivery', 'Api\OrderController@assignOrderToDelivery');
    Route::get('order/{order}/print', 'Api\PrintOrder');
});
