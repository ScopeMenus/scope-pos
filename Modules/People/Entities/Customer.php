<?php

namespace Modules\People\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function newFactory() {
        return \Modules\People\Database\factories\CustomerFactory::new();
    }

//    public function getPhoneAttribute($value)
//    {
//        return explode(',', $value);
//    }

    public function updatePhoneAttribute($value)
    {
        $phones = explode(',', $this->attributes['phone']);

        array_push($phones, $value);

        return implode(',', $phones);
    }

    public function deletePhoneAttribute($value)
    {
        $phones = explode(',', $this->attributes['phone']);

        $search = array_search($value, $phones);

        if($search)
            unset($phones[ array_search($value, $phones) ]);

        return implode(',', $phones);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
}
