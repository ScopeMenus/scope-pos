<?php

namespace Modules\People\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RespondActive;
use Illuminate\Http\Request;
use Modules\People\Entities\Address;
use Modules\People\Entities\Customer;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(Request $request)
    {
        $address = Address::create([
            'customer_id'   => $request->customer_id,
            'flat'          => $request->flat,
            'floor'         => $request->floor,
            'building'      => $request->building,
            'special_sign'  => $request->special_sign,
            'full_address'  => $request->full_address,
            'shipping'      => $request->shipping,
        ]);

        return RespondActive::success('The action ran successfully!', $address);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Address $address
     * @return
     */
    public function update(Request $request, Address $address)
    {
        $address->update([
            'flat'          => $request->flat,
            'floor'         => $request->floor,
            'building'      => $request->building,
            'special_sign'  => $request->special_sign,
            'full_address'  => $request->full_address,
            'shipping'      => $request->shipping,
        ]);

        return RespondActive::success('The action ran successfully!', $address);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Customer $customer
     * @return
     */
    public function destroy(Address $address)
    {
        $address->delete();

        return RespondActive::success('The action ran successfully!');
    }
}
