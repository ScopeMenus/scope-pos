<?php

namespace Modules\People\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RespondActive;
use Illuminate\Http\Request;
use Modules\People\Entities\Customer;
use Modules\People\Http\Requests\StoreCustomerRequest;
use Modules\People\Http\Resources\CustomerResource;

class CustomerController extends Controller
{
    public function index()
    {
        $customer =
        Customer::query()
        ->where('phone', 'like', '%'.request()->search.'%')
        ->with('addresses')
        ->first(['id', 'name', 'phone']);

        if (!$customer) {
            return RespondActive::clientError('User not found!');
        }

        return RespondActive::success('The action ran successfully!', $customer);
    }

    public function store(StoreCustomerRequest $request)
    {
        $customer = Customer::create([
            'branch_id' => auth()->user()->branch_id,
            'name'      => $request->name,
            'phone'     => $request->phone,
        ]);

        $customer->addresses()->create([
            'flat'          => $request->flat,
            'floor'         => $request->floor,
            'building'      => $request->building,
            'special_sign'  => $request->special_sign,
            'full_address'  => $request->full_address,
            'shipping'      => $request->shipping,
        ]);

        $customer = new CustomerResource($customer);

        return RespondActive::success('The action ran successfully!', $customer);
    }

    public function update(Request $request, Customer $customer)
    {
        if ($request->phone) {
            $customer->update([
                'phone'  => $customer->updatePhoneAttribute($customer->phone)
            ]);

            $data = $customer->phone;
        }

        return RespondActive::success('The action ran successfully!', $data);
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();

        return RespondActive::success('The action ran successfully!');
    }
}
