<?php

namespace Modules\People\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RespondActive;
use Modules\People\Entities\Customer;
use Modules\People\Http\Requests\PhoneRequest;

class PhoneController extends Controller
{
    public function store(PhoneRequest $request)
    {
        $customer = Customer::find($request->customer_id);

        $customer->update([
            'phone'  => $customer->updatePhoneAttribute($request->phone)
        ]);

        $data = $customer->phone;

        return RespondActive::success('The action ran successfully!', $data);
    }

    public function destroy(PhoneRequest $request)
    {
        $customer = Customer::find($request->customer_id);

        $customer->update([
            'phone'  => $customer->deletePhoneAttribute($request->phone)
        ]);

        return RespondActive::success('The action ran successfully!');
    }
}
