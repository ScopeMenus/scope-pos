<?php

namespace Modules\People\Http\Requests;

use App\Services\RespondActive;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => ['required', 'string'],
            'phone'         => ['required', 'string', 'unique:customers'],
            'flat'          => ['required', 'numeric'],
            'floor'         => ['required', 'numeric'],
            'building'      => ['required', 'string'],
            'special_sign'  => ['nullable', 'string'],
            'full_address'  => ['required', 'string'],
            'shipping'      => ['required', 'string'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(RespondActive::clientError(
            RespondActive::stringifyErrors($validator->errors())
        ));
    }
}
