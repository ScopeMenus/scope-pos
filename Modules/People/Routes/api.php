<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/people', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function() {
    Route::apiResource('customer', 'Api\CustomerController');

    Route::apiResource('address', 'Api\AddressController');

    Route::post('phone', 'Api\PhoneController@store');
    Route::delete('phone', 'Api\PhoneController@destroy');
});

