<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('branch_id')->nullable()->references('id')->on('shops')->nullOnDelete();
            $table->unsignedBigInteger('category_id');
            $table->string('product_name');
//            $table->string('barcode')->unique()->nullable();
            $table->integer('product_quantity');
            $table->integer('product_price');
            $table->integer('product_stock_alert');
            $table->integer('product_selling_count')->default(0);
            $table->foreign('category_id')->references('id')->on('categories')->restrictOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
