<?php

namespace Modules\Product\Http\Controllers\Api;

use App\Services\RespondActive;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Category;
use Modules\Product\Http\Resources\CategoryResource;

class CategoryController extends Controller
{
    public function __invoke()
    {
        $categories = Category::with('products')->get();

        $data['categories'] = CategoryResource::collection($categories);

        $data['config'] = [ 'service' => 12.5 , 'tax' => 13 ];

        return RespondActive::success('success', $data);
    }
}
