<?php

namespace Modules\Product\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdditionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->product_name,
            'quantity'      => $this->product_quantity,
            'price'         => $this->product_price,
            'image'         => optional($this->media()->first())->original_url,
            'additions'     => []
        ];
    }
}
