<?php

namespace Modules\Table\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Order\Entities\Order;

class Table extends Model
{
    use HasFactory;

    protected $guarded = [];

    const AVAILABLE = 0;
    const BUSY = 1;
    const RESERVED = 2;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
