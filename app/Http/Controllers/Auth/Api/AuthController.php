<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\UserLoginRequest;
use App\Models\User;
use App\Services\RespondActive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(UserLoginRequest $request)
    {
        $user = User::whereName($request->name)->checkUserType('cashier')->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return RespondActive::clientError('Wrong name or password.');
        }

        $user['token'] = $user->createToken($request->name)->plainTextToken;

        return RespondActive::success('Logged in successfully.', $user);
    }

    public function logout(Request $request)
    {
        User::find($request->user_id)->findOrFail()->logout();

        return RespondActive::success('Logged out successfully.');
    }
}
