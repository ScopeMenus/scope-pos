<?php

namespace App\Http\Livewire\Reports;

use Livewire\Component;
use Livewire\WithPagination;
use Modules\Order\Entities\Order;

class OrdersReport extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $start_date;
    public $end_date;

    protected $rules = [
        'start_date' => 'required|date|before:end_date',
        'end_date'   => 'required|date|after:start_date',
    ];

    public function mount() {
        $this->start_date = today()->subDays(30)->format('Y-m-d');
        $this->end_date = today()->format('Y-m-d');
    }

    public function render() {
        $orders = Order::whereDate('created_at', '>=', $this->start_date)
            ->whereDate('created_at', '<=', $this->end_date)
            ->with('cashier')
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('livewire.reports.orders-report', [
            'orders' => $orders
        ]);
    }

    public function generateReport() {
        $this->validate();
        $this->render();
    }
}
