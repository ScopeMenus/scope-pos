<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Modules\Setting\Entities\Setting;

class CheckRequestsTimestamp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (now()->gt(settings()->last_request) &&
            now()->gt(settings()->start_subscription) &&
            now()->lt(settings()->end_subscription)) {

            Setting::first()->update([
               'last_request'   => now()
            ]);

            cache()->forget('settings');

            return $next($request);
        }

        return response()->json('Error', 403);
    }
}
