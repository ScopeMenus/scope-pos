<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->string('shop_name');
            $table->string('owner_name');
            $table->string('owner_phone', 100)->nullable();
            $table->string('phone', 100);
            $table->string('additional_phone', 100)->nullable();
            $table->string('contracted_person_name', 100);
            $table->string('commercial_register_photo')->nullable();
            $table->string('tax_card_photo')->nullable();
            $table->timestamp('contacted_at')->nullable();
            $table->timestamp('contract_expiry_date')->nullable();
            $table->float('contract_cost', 8, 2);
            $table->foreignId('city_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('region_id')->nullable()->constrained()->nullOnDelete();
            $table->string('address')->nullable();
            $table->string('social_media')->nullable();
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
