<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNegotiationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('negotiations', function (Blueprint $table) {
            $table->id();
            $table->string('negotiating_name');
            $table->string('phone', 100);
            $table->string('additional_phone', 100)->nullable();
            $table->string('follow_negotiate_name', 100);
            $table->timestamp('contacted_at');
            $table->float('time_duration', 8, 2);
            $table->string('shop_name');
            $table->text('negotiate_notes')->nullable();
            $table->foreignId('city_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('region_id')->nullable()->constrained()->nullOnDelete();
            $table->string('address');
            $table->string('social_media')->nullable();
            $table->string('majority')->nullable();
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('negotiations');
    }
}
