
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Receipt example</title>
</head>
<body>
<div class="ticket" id="GFG" style="display:none;">
    <img src="./logo.png" alt="Logo">
    <p class="centered">RECEIPT EXAMPLE
        <br>Address line 1
        <br>Address line 2</p>
    <table>
        <thead>
        <tr>
            <th class="quantity">Q.</th>
            <th class="description">Description</th>
            <th class="price">$$</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="quantity">1.00</td>
            <td class="description">بيبسي</td>
            <td class="price">$25.00</td>
        </tr>
        <tr>
            <td class="quantity">2.00</td>
            <td class="description">شيبسي</td>
            <td class="price">$10.00</td>
        </tr>
        <tr>
            <td class="quantity">1.00</td>
            <td class="description">STICKER PACK</td>
            <td class="price">$10.00</td>
        </tr>      <tr>
            <td class="quantity">1.00</td>
            <td class="description">STICKER PACK</td>
            <td class="price">$10.00</td>
        </tr>      <tr>
            <td class="quantity">1.00</td>
            <td class="description">STICKER PACK</td>
            <td class="price">$10.00</td>
        </tr>      <tr>
            <td class="quantity">1.00</td>
            <td class="description">STICKER PACK</td>
            <td class="price">$10.00</td>
        </tr>      <tr>
            <td class="quantity">1.00</td>
            <td class="description">STICKER PACK</td>
            <td class="price">$10.00</td>
        </tr>      <tr>
            <td class="quantity">1.00</td>
            <td class="description">STICKER PACK</td>
            <td class="price">$10.00</td>
        </tr>
        <tr>
            <td class="quantity"></td>
            <td class="description">TOTAL</td>
            <td class="price">$55.00</td>
        </tr>
        </tbody>
    </table>
    <p class="centered">Thanks for your purchase!
        <br>parzibyte.me/blog</p>
</div>
<input type="button" value="click" onclick="printDiv()">

</body>
<script>
    function printDiv() {
        var divContents = document.getElementById("GFG").innerHTML;
        var a = window.open('', '', 'height=1000, width=1000');
        a.document.write('<html>');
        a.document.write(`
        <body>
            <style>
            * {
                font-size: 12px;
            }

            td,
            th,
            tr,
            table {
                border-top: 1px solid black;
                border-collapse: collapse;
            }

            td.description,
            th.description {
                width: 75px;
                max-width: 75px;
            }

            td.quantity,
            th.quantity {
                width: 40px;
                max-width: 40px;
                word-break: break-all;
            }

            td.price,
            th.price {
                width: 40px;
                max-width: 40px;
                word-break: break-all;
            }

            .centered {
                text-align: center;
                align-content: center;
            }

            .ticket {
                width: 155px;
                max-width: 155px;
            }

            img {
                max-width: inherit;
                width: inherit;
            }

            @media print {
                .hidden-print,
                .hidden-print * {
                    display: none !important;
                }
            }
        </style>`);
        a.document.write(divContents);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        a.close();
    }
</script>
</html>
